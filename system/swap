#!/bin/bash 

# Get current swap usage for all running processes, from Erik Ljungstrom 27/05/2011
# rewrite by kyodev

#~ version=0.4
#~ date=26.12.2018

######## fonctions ---------------------------------------------------------------------------------

	# $1: message $2:défaut(oui|non), return 0 pour oui ou 1 pour non
f__dialog_simple(){	# 26.12.2018
	local message="$1" defaut="$2" disp reponse

	[[ ${defaut,,} == oui ]] && disp="(O/n)" || disp="(o/N)"
	echo -n "  $message $disp "
	read -r 
	tput cuu1				# une ligne plus haut
	tput dl1				# efface ligne
	[[ ${REPLY,,} != o && ${REPLY,,} != n ]] && reponse="$defaut"		# N non par défaut
	[[ ${REPLY,,} == o ]] && reponse="oui"
	[[ ${REPLY,,} == n ]] && reponse="non"
	[[ $reponse == oui ]] && return 0 || return 1
}

	# $1: swappiness
swappiness_config(){	# 26.12.2018
	local reponse

	echo -n "  swappiness par défaut? (20 par défaut) "
	read -r 
	tput cuu1				# une ligne plus haut
	tput dl1				# efface ligne
	reponse=${REPLY:=20}	# 20 par défaut si saisie vide
	if (( reponse < 10 || reponse > 60 )); then
		echo "hors bornes (10 à 60)"
		return 1
	fi

	echo "$reponse" > /proc/sys/vm/swappiness
	if grep -Eq 'vm.swappiness' /etc/sysctl.conf; then
		sed -Ei 's/(vm.swappiness[[:blank:]]*=[[:blank:]]*)([[:digit:]]{2})/\1'"$reponse"'/' /etc/sysctl.conf
	else
		echo $'\n'"vm.swappiness = $reponse"$'\n' >> /etc/sysctl.conf
	fi
}

usage(){	# 26.12.2018

	cat <<- end

	  ${GREEN}$( basename "$0" ) [ options ]$STD
	      script seul : affichage des $qteToDisplay plus fortes utilisations de la swap
	                    propose nettoyage mémoire et redémarrage swap
	                    affiche le swappiness 
	                    permet de le configurer (pour la session en cours et dans la config système)
	                    (root requis & suffisamment de mémoire dispo pour libérer la swap)

	  ${GREEN}options:$STD
	      ${BLUE}-a, --all :$STD exécute le script, avec nettoyage mémoire et redémarrage swap 
	                  (root requis & suffisamment de mémoire dispo pour libérer la swap)
	      ${BLUE}-h, --help :$STD cette aide

	end
}

utilisation_swap(){	# 26.12.2018
	local sum=0 overall=0 dir pid progName a swap b recap sortie

	shopt -s nullglob
	for dir in /proc/* ; do
		[[ ! $dir =~ ^/proc/[0-9] ]] && continue	# exclusion des "proc" non numériques
		pid=${dir##*\/}
		progName=$( ps -p "$pid" -o comm --no-headers )
		while read -r a swap b; do
			(( sum+=swap ))
		done <<< "$( grep -w 'Swap:' "$dir/smaps" 2>/dev/null )"
		echo "$a $b" > /dev/null					# évite erreur SC2034: shellcheck
		(( sum > 0 )) && recap+=$( printf "  %8d ko en swap, pid: %5d (%s)\\n" "$sum" "$pid" "$progName" )$'\n'
		(( overall+=sum ))
		sum=0
	done
	shopt -u nullglob

	echo $'\n'"  swap utilisée: $( printf "%'d ko" $overall )"$'\n'

		# affichage extrait des utilisations swap
	sortie=$( sort -nr -k1 <<< "$recap" | head -n"$qteToDisplay" )
	if [[ $sortie ]]; then
		echo "$sortie"
		echo
	fi
}

vidage_memoire(){	# 26.12.2018

	#~ sysctl -w vm.drop_caches=3	à voir
	#~ sync ; echo 1 > /proc/sys/vm/drop_caches		# libération pagecache
	#~ sync ; echo 2 > /proc/sys/vm/drop_caches		# libération dentries et inodes
	sync ; echo 3 > /proc/sys/vm/drop_caches		# libération pagecache, dentries et inodes
}

vidage_swap(){		# 26.12.2018

	swapoff -a && swapon -a
}

######## paramètres --------------------------------------------------------------------------------

qteToDisplay=10		# nombre de processus en swap à afficher


######## script ------------------------------------------------------------------------------------

if [[ $1 =~ -h ]]; then
	usage
	exit 0
fi

while read -r; do
	swaps="$REPLY"		# dernière ligne de swaps
done <<< $( < /proc/swaps )
[[ $swaps =~ ilename ]] && noSwap='yes'		# uniquement ligne entête => pas de swap

if [[ ! $1 =~ -a && -z $noSwap ]]; then
	utilisation_swap

		# swappiness en cours
	echo "  Swappiness en cours: $(< /proc/sys/vm/swappiness)"$'\n'
fi

	# sortie si pas de droits root
if (( EUID != 0 )); then
	echo $'\n'"  droits root requis pour aller plus loin (nettoyage mémoire et vidage swap)"$'\n'
	exit 0
fi

	#  vidage mémoire
reponse='oui'
if [[ ! $1 =~ -a ]]; then	# en manuel, on pose la question
	f__dialog_simple "nettoyer la mémoire?" "$reponse" && reponse='oui' || reponse='non'
fi
[[ ${reponse,,} == 'oui'  ]] && vidage_memoire

	# pas de swap, inutile d'aller plus loin
if [[ $noSwap ]]; then
	exit 0
fi

	# vidage swap
reponse='oui'
if [[ ! $1 =~ -a ]]; then	# en manuel, on pose la question
	f__dialog_simple "essayer de vider la swap?" "$reponse" && reponse='oui' || reponse='non'
fi
[[ ${reponse,,} == 'oui'  ]] && vidage_swap

	# réglage swappiness, en manuel
reponse='non'
if [[ ! $1 =~ -a ]]; then	# en manuel, on pose la question
	f__dialog_simple "modifier swappiness?" "$reponse" && reponse='oui' || reponse='non'
fi
[[ ${reponse,,} == 'oui'  ]] && swappiness_config
