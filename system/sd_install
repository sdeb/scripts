#!/usr/bin/env bash

# sd_install: install de scripts distants sur /opt/bin
#
# script, url et éventuelle fonction spécifique dans /opt/bin/sd_install.conf

##### license LPRAB/WTFPL
#  auteur: sdeb
#    contributeurs: kyodev
#####

script=$( basename $0 )
version=1.5.0
date=03/07/2018

### fonctions utilités ---------------------------------------------------------

	# $1 message to display
f__die(){		# 20/06/2018

	echo -e "\\n  ${RED}Fatal$STD: $1\\n"
	exit 1
}

	# recherche commandes/paquets, $1 liste: cmd1|cmd2[>paquet] (séparées par espaces) ex: "gawk|mawk>gawk wget"
	# si manque, return 1 & affiche commandes manquantes (si debian, ajout proposition paquet à installer)
	# requiert f__sort_uniq
f__requis(){		# 30/06/2018
	local ENV_DEBIAN c_echo ireq cmds package commands command 
	local commandsMissing packagesMissing t_commandsMissing t_packagesMissing

	if type -p dpkg &>/dev/null ; then
		ENV_DEBIAN="oui"							# debian
	fi
	if type -t f__info &>/dev/null; then
		c_echo="f__info"
	else
		c_echo="echo -e"					# f__info existe? sinon echo
	fi
	for ireq in $1; do
		IFS='>' read -r cmds package <<< "$ireq"			# ex: "awk|gawk>gawk wget file tar killall>psmisc"
		IFS='|' read -ra commands <<< "${cmds}"
		[ -z "$package" ] && package=${commands[0]}	# pas de packages dans les options, donc = commands[0]
		unset t_commandsMissing t_packagesMissing
		for command in "${commands[@]%$'\n'}"; do
			if type -p "$command" &>/dev/null ; then
				unset t_commandsMissing t_packagesMissing
				break
			else	# inexistant
				t_commandsMissing+="$command "
				t_packagesMissing="$package "
			fi
		done
		[ "$t_commandsMissing" ] && commandsMissing+="$t_commandsMissing "
		[ "$t_packagesMissing" ] && packagesMissing+="$t_packagesMissing "
	done

		# dédoublonnage & tri
	commandsMissing=$( f__sort_uniq "$commandsMissing" )
	packagesMissing=$( f__sort_uniq "$packagesMissing" )

		# affichage final
	if [ "$commandsMissing" ] && [ "$ENV_DEBIAN" ]; then
		$c_echo "${RED}erreur critique, manquant: $STD$BOLD$commandsMissing$STD" \
						"vous devriez exécuter:$GREEN apt install $packagesMissing$STD"
	elif [ "$commandsMissing" ]; then
		$c_echo "${RED}erreur critique, manquant: $STD$BOLD$commandsMissing$STD"
	fi

	[ "$commandsMissing" ] && return 1 || return 0
}

	# options: liste à trier [-lf], affiche flat liste (\n enlevés en sortie) ou si -ln  liste avec \n à chaque ligne
	# des caractères non permis =$/`... sont transformés en ø
f__sort_uniq(){		# 02/07/2018
	local lf list array result item

	while (( $# )) ; do
		case "$1" in
			-lf ) lf='yes' ;;
			 * ) list=${1} ;;
		esac
		shift
	done

	while read -r ; do		# lecture ligne
		[ -z "$REPLY" ] && continue
		REPLY=${REPLY# } 		# trim gauche simple
		REPLY=${REPLY% }		# trim droite simple
		REPLY=${REPLY// /\~}	# espace remplacé avec ~
			# interdits/remplacés: ()/`;      ø utilisé comme indicateur, ~ remplacé pas espace
		REPLY=${REPLY//[^a-zA-Z0-9~.,\{\}!\[\]#*%µ£°⇉:_+-]/ø}	# caractère <> de autorisé remplacés par ø 
		if [ "$lf" ]; then
			array+=( "${REPLY}" )
		else
			IFS='~' read -ra array <<< "$REPLY"
		fi
	done <<< "${list}"

	result=$(
		for item in "${array[@]}"; do
			eval "alias ${item}=ls"
		done
		alias
	)
	result=${result//alias }
	result=${result//=\'ls\'}
	result=${result//\~/ }
	[ "$lf" ] && echo "${result}" || echo "${result//$'\n'/ }"
}

	# $@=cmd à lancer en root avec su ou sudo. si sudo possible: prioritairesu su
f__sudo(){		# 11/06/2018

	if sudo -v &>/dev/null && [ $EUID -ne 0 ] ; then
		sudo su --preserve-environment -c "$@"
	else
		echo -n "[su]   "
		su --preserve-environment -c "$@"
	fi
}

	# user ayant initié la session graphique, assigne $fu_user
	# return 1 sur échec identification user, return 2 sur absence home/
	# gestion variable environnement user avec: USER_INSTALL=<user> script
f__user(){		# 08/03/2018
	local user userid root_login

	root_login="$(grep ':0:' /etc/passwd | cut -d':' -f1)" || root_login="root"
	if [ "$USER_INSTALL" ]; then 	# user via variable environnement, moyen d'injecter root si pb 
		fu_user="$USER_INSTALL"; 
		return 0
	elif [[ "$TERM" =~ linux ]]; then	#debian 9 recovery ou nomodeset TERM=linux
		if [ "$USER" ]; then
			user="$USER"
		elif [ "$EUID" -eq 0 ]; then
			fu_user="$root_login"
			return 0
		fi
	fi
	if [ "$SUDO_UID" ]; then 
		userid="$SUDO_UID"; 
	elif grep -qEo '[0-9]+' <<< "$XDG_RUNTIME_DIR" ; then 
		userid=$( grep -Eo '[0-9]+' <<< "$XDG_RUNTIME_DIR" | cut -d'/' -f4 )
	else
		userid=$( grep -Eo '[0-9]+' <<< "$XAUTHORITY" | cut -d'/' -f4 )
	fi
	[ "$userid" ] && user=$( grep "$userid" /etc/passwd | cut -d ":" -f 1 )
	if [ "$user" ] && [ "$user" != "$root_login" ]; then
		fu_user="$user"
		return 0
	else
		if [ "$SUDO_USER" ] && [ "$SUDO_USER" != "$root_login" ]; then 
			user="$SUDO_USER"; 
		elif who | grep -qv 'root'; then 
			user=$( who | grep -v 'root' | head -n1 | cut -d ' ' -f1 );	# who | grep -v 'root' | awk 'FNR==1{print $1}'
		else
			user=$( grep -m1 'hourly.*get[A-Z].*\.anacrontab.*\.config/anacron/spool' /etc/crontab | cut -d' ' -f2 );
		fi
	fi
	fu_user="$user"
	[ "$fu_user" ] || return 1
	[ -d "/home/$fu_user" ] || return 2
	return 0
}

	# test wget, $1=url à tester, $2=''|print|loc|test
	# par défaut, sortie du script (même si url testée ok) avec affichage erreur ou ok
	# si $2=print affiche url testée & entêtes http & location, return 0
	# si $2=loc affiche seulement location, return 0
	# si $2=test return 0 si ok, return 1 si KO
f__wget_test(){		# 07/06/2018
	local file_test_wget="/tmp/testWget-$script" retourWget retourHttp location

	wget -Sq --timeout=5 --tries=2 --user-agent="$user_agent" --spider --save-headers "$1" &>"$file_test_wget"
	retourWget="$?"
	[ "$retourWget" == 1 ] && retourWget="1: code erreur générique"
	[ "$retourWget" == 2 ] && retourWget="2: parse erreur (ligne de commande?)"
	[ "$retourWget" == 3 ] && retourWget="3: erreur Entrée/sortie fichier"
	[ "$retourWget" == 4 ] && retourWget="4: défaut réseau"
	[ "$retourWget" == 5 ] && retourWget="5: défaut vérification SSL"
	[ "$retourWget" == 6 ] && retourWget="6: défaut authentification"
	[ "$retourWget" == 7 ] && retourWget="7: erreur de protocole"
	[ "$retourWget" == 8 ] && retourWget="8: réponse serveur en erreur"
	retourHttp=$( grep -i 'HTTP/' "$file_test_wget" | tr -d '\n' | xargs )
	location=$( grep -i 'location' $file_test_wget | xargs )
	if [ "$2" == "test" ]; then
		rm -f "$file_test_wget"
			# spécial maintenance frama.link, pas de redirection sur page status framalink
		grep -q '303' <<< "$retourHttp" && return 1			# 303 See Other
		[ "$retourWget" == "0" ] && return 0 || return 1
	fi
	if [ "$2" == "print" ]; then
		if [ "$retourWget" != "0" ]; then
			echo "  erreur wget: erreur $RED$retourWget" 
			echo -e "$BLUE  $1$STD\\t$RED  $retourHttp$STD"
		else
			echo -e "$BLUE  $1$STD\\t$GREEN  $retourHttp$STD" 
		fi
	fi
	if [ "$2" == "print" ] || [ "$2" == "loc" ]; then
		[ "$location" ] && echo -n "$YELLOW  $location" || echo -n "$YELLOW  no location"
		echo "$STD"
		rm -f "$file_test_wget"
		return 0
	fi
	if [ "$retourWget" != "0" ]; then
		rm -f "$file_test_wget"
		f__error "wget, erreur $retourWget" "$1" "$YELLOW$retourHttp" "$location"
		echo -e "$RED   erreur wget, $retourWget \\n  $1 \\n  $YELLOW$retourHttp \\n  $location$STD" # pour les diags
		return 1
	fi
	if grep -q '200' <<< "$retourHttp"; then 
		echo -e "$GREEN\\ntout est ok, réessayer$STD\\n"
	fi
	rm -f "$file_test_wget"
	exit 0
}


### fonctions script -----------------------------------------------------------

	# DL dans rep. temporaire, $1=script à charger, $2=url à charger
f_dl(){		# 02.01.2018
	local wget_log="/tmp/wget_$RANDOM.log"

	wget -q --tries=2 --timeout=15 --user-agent="$user_agent"  -o "$wget_log" -O "$dir_temp/$1" "$2"
	if [ "$?" -ne 0 ]; then		# en cas d'erreur
		rm -f "$dir_temp/$1"
		rm -f "$wget_log"
		f__wget_test "$2"
		exit
	fi
}

	# [$1=quiet], assigne $ver_script_online, $ver_script_enplace, $script_a_jour=ok|KO, return 1 si script_a_jour KO 
f_get_versions(){		# 03.01.2018

	ver_script_online=$( wget -q --timeout=10  -O - "$url_script" | grep -m1 '^version=' | cut -d'=' -f2 )
	rm -f "wget.log"					# effacement éventuel wget.log parasite (bug gnome?)
	ver_script_enplace=$( grep -m1 '^version=' $dir_install/$script | cut -d'=' -f2 )
	if [ "$ver_script_online" != "$ver_script_enplace" ]; then
		script_a_jour="KO"
		return 1
	else
		script_a_jour="ok"
	fi
	[ "$1" == "quiet" ] && return 0

	echo -e "script en place: $GREEN$ver_script_enplace"
	echo -e "script en ligne: $YELLOW$ver_script_online"
}

f_help(){		# 03.01.2018
	local liste=${!script_dl[*]}

	liste=$( sort <<< ${liste// /$'\n'})	# bug tableaux associatifs, sortie non ordonnées, donc tri
	liste=${liste//$'\n'/ - }
	echo "------------------------------------------"
	echo "$script i-<script> : installation <script>"
	echo "$script r-<script> : suppression <script>"
	echo
	echo "$script -h : help"
	echo "$script -v : versions $script"
	echo "------------------------------------------"
	echo "$script, en standard:"
	echo "  . télécharge un script"
	echo "  . l'installe dans /opt/bin"
	echo "  . ajoute un lien sur /usr/bin"
	echo "  . le rend exécutable"
	echo "  . exécute un éventuel script post-install"
	echo "$script, spécifiquement, peut juste copier dans /opt/bin"
	echo
	echo "voir /opt/bin/$script.conf pour la liste des scripts configurés ou des détails sur la configuration possible"
	echo "------------------------------------------"
	echo "scripts installables : $liste"
}

	# copie de dir temp à dir installation, chmod et link sur /usr/bin/,  $1=$script [$2=options: noExec]
	# noExec, juste copie de dir temp à dir install, pas de link /bin, pas de message
f_install(){		# 03.01.2018
	local script_to_install="$1" option="*2"

	mkdir -p "$dir_install"
	cp -d "$dir_temp/$script_to_install" "$dir_install/"
	if [ ${option} != "noExec" ]; then
		chmod 775 "$dir_install/$script_to_install"	# rwx rwx r-x, proprio root
		ln -s "$dir_install/$script_to_install" "/usr/bin/$script_to_install" &>/dev/null
		echo "$script_to_install installé dans $dir_install"
	fi
	rm -f "$dir_temp/$script_to_install"
}

f_maj_conf(){		# 03.01.2018

	f_dl "$file_conf" "$url_file_conf"
	f_install "$file_conf" noExec
}

	# $1=script_to_install
f_remove(){		# 03.01.2018
	local script_to_remove="$1"

	if [ -e "$dir_install/$script_to_remove" ]; then
		rm -f "$dir_install/$script_to_remove"
		unlink "/usr/bin/$script_to_remove" &>/dev/null
		echo "$script_to_remove effacé du système"
	else
		echo "script $script_to_remove inconnu"
	fi
}

	# auto install, return 1 si maj faite (script à relancer)
f_script_install(){		# 03.01.2018

	if [[ ! "$( dirname $0 )" =~ /bin ]]; then		# emplacement n'est pas dans un /bin
		echo "installation $script"
		cp "$0" "$dir_temp/"						# pour gestion normale par f_install
		f_install "$script"
		if [ ! -e "$fileDev" ]; then
			rm -f "$0"								# efface si pas fileDev ( touch /opt/bin/fileDev )
		fi
		return 1
	fi
}

	# maj si version en ligne plus récente
f_script_maj(){		# 03.01.2018

	if ! f_get_versions "quiet"; then
		echo "maj $script"
		f_dl "$script" "$url_script"
		f_install "$script"
		return 1
	fi
}


### paramètres script ----------------------------------------------------------

user_agent="Mozilla/5.0 Firefox"
fileDev="/opt/bin/fileDev"
file_conf="$script.conf"
dir_install="/opt/bin"
url_script="https://framagit.org/sdeb/scripts/raw/master/system/sd_install"
url_file_conf="https://framagit.org/sdeb/scripts/raw/master/system/sd_install.conf"
dir_temp="/tmp/$script"
if env | grep 'XDG_.*ubuntu'; then
	os_env="ubuntu"
elif type -p dpkg &>/dev/null; then 
	os_env="debian"
fi

### préliminaires script -------------------------------------------------------

if [ "$EUID" -ne 0 ]; then		# acquiert droit root si besoin, pour install ou remove
	f__sudo "exec $0 $@"
	exit $?
fi

mkdir -p "$dir_temp"									# dir_temp emplacement des DL et sources des installs
f_script_install || exec $dir_install/$script $@		# install si besoin
f_script_maj || exec $dir_install/$script $@			# maj si besoin
f_maj_conf												# DL systématique de conf

source "$dir_install/$file_conf"						# lecture fichier conf

	# traitement options
options=$@
if [[ "$options" =~ ^- ]]; then
	action=${options#?-}
else
	action=${options%-*}
	script_to_install=${options#?-}
	url_dl=${script_dl[$script_to_install]}
fi

### start script ---------------------------------------------------------------

echo -e "\n\t$script version $version\n"

case "$action" in 
	i )
		if [ "$url_dl" ]; then 								# si script connu
			f__requis "wget"			# paquet/commande requis
			f__user
			f_dl "$script_to_install" "$url_dl"
			f_install "$script_to_install" ${script_action[$script_to_install]}
				# fonction spécifique
			if [ ${script_todo[$script_to_install]} ]; then
				echo "${script_todo[$script_to_install]}()"
				${script_todo[$script_to_install]}			# fonction spécifique lancée si existante
			fi
		else 
			echo "script $script_to_install inconnu"
			action="-h"
		fi
		;;
	r )
		f_remove "$script_to_install" ;;
	-v )
		f_get_versions ;;
	-h | * )
		f_help ;;
esac

rm -fr "$dir_temp"
echo

exit

méthode à tester:
wget -nv -O -  https://framagit.org/sdeb/scripts/raw/master/system/sd_install | bash

méthode historique:
wget -nv -O sd_install  https://framagit.org/sdeb/scripts/raw/master/system/sd_install
chmod +x sd_install && ./sd_install
