Pour fonctionner tel qu'il se doit ce script doit:

-Être sur une debian ou une debian-based avec un cron installé (c'est le cas par default normalement avec anacron)

-Pour une mise à jour quotidienne, le mettre dans /etc/cron.daily

-Pour une mise à jour hebdomadaire, le mettre dans /etc/cron.weekly

-Le rendre exécutable avec "chmod +x autoUpdate"

-Enfin pour forcer le cron pour être sûr de son fonctionnement, faire "run-parts /etc/cron.daily" ou "run-parts /etc/cron.weekly" suivant l'endroit ou vous l'avez placé.

Ce script aura sûrement un installeur dans le futur
