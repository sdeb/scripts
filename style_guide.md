## général

* utf-8
* indentation: TABULATION
* $() pas \`\`
* pas de commandes séquentielle avec ;
* pas de "_conditions compactes_" **(condition && action1 || action2)**, par lisibilité, lors affectation variables, au moins pour la première affectation, donc if then else multilignes
* pas de "_conditions compactes_" sur des action importantes/dangereuses, en particulier si action1 peut être en erreur et induire donc, aussi, action 2, donc if then else multilignes
* _éventuellement_, remplacer condition compactes et simples par if then else sur une ligne pour éviter warning shellcheck
* == pour marquer les équivalences, = pour les assignations, bien que = pourraient être utilisées pour 
  les équivalences
* même si '[[ ]]' permettrait d'éviter les guillemets, il sont quand même mis par convention, ne serait ce que 
  pour une coloration syntaxique homogène
* shellscheck utilisé systématiquement


## fonctions

sauf exception (initialisation), les fonctions utilisent ce principe de nommage:

* f__	 :	fonctions utilitaires, communes à plusieurs scripts
    * variables publiques: pas de conventions de nommage pour l'instant (à faire)
        * à faire : fu_ (Utilité)

* f_	 : fonction généraliste propre au script
    * variables publiques: AUCUNE

* fi_	 :	affichage/présentation
    * variables publiques: $fe_ (Exception)
    * éviter les variables publiques
    * exceptions: sous-fonctions, sortie pour lisibilité, variables n'a pas à respecter le nommage (sous-fonction)

* figet_ :	calculs/préparation
    * variables publiques: $fg_ (Général)
    * aucun affichage


## variables

* toutes les variables internes à la fonction doivent être déclarées en local en tête de fonction
* les variables publiques sont signalées en première lignes de commentaire au-dessus de la déclaration de fonction
* nommage
    * fg_ 	: 	pour une variable de fonction figet_
    * fe_	:	pour une variable de fonction f_ (attention aux collisions avec un nom de fonction)


## à traduire/intégrer
	####  * Always use quotes, double or single, for all string values.
	####  * All new code/methods must be in a function.
	####  * For all boolean tests, use 'true' | 'false', Do NOT use 0 or 1 unless it's a function return. 
	####  * Avoid complicated tests in the if condition itself.
	####  * To 'return' a value in a function, use 'echo <var>'.
	####  * For gawk: use always if ( num_of_cores > 1 ) { hanging { starter for all blocks
	####    This lets us use one method for all gawk structures, including BEGIN/END, if, for, etc
	####  SUBSHELLS ARE EXPENSIVE! - run these two if you do not believe me.
	####  time for (( i=0; i<1000; i++ )) do ff='/usr/local/bin/foo.pid';ff=${ff##*/};ff=${ff%.*};done;echo $ff
	####  time for (( i=0; i<1000; i++ )) do ff='/usr/local/bin/foo.pid';ff=$( basename $ff | cut -d '.' -f 1 );done;echo $ff
	~~####  * Using ${VAR} is about 30% slower than $VAR because bash has to check the stuff for actions~~  
		tests actuel: environ 3%
